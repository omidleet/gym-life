import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from '../components/about/about.component';
import { UseCaseListComponent } from '../components/use-case-list/use-case-list.component';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { HomeComponent } from '../components/home/home.component';

const routes: Routes = [
  { path: '', 
    redirectTo: 'home',
    pathMatch: 'full'},

  { path: 'home', component: HomeComponent},  

  { path: 'navbar', component: NavbarComponent },

  { path: 'about', component: AboutComponent},

  { path: 'use-case-list', component: UseCaseListComponent }, 


];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
      RouterModule
  ],
})
export class AppRoutingModule { }
export const routingComponents = [AboutComponent, UseCaseListComponent, NavbarComponent, HomeComponent]